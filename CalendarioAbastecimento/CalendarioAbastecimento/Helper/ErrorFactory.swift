//
//  ErrorFactory.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

enum ErrorCode : Int{
    case noConnection = 5
    case serviceError = 20
    case noAddressError = 35
}

class ErrorFactory: NSObject{
    
    static func noConncectionError() -> NSError {
        return NSError(domain: ERR_noInternetConnectionMessage, code: ErrorCode.noConnection.rawValue, userInfo: nil)
    }
    
    static func serviceError() -> NSError {
        return NSError(domain: ERR_serviceErrorMessage, code: ErrorCode.serviceError.rawValue, userInfo: nil)
    }
    
    static func noAddressError() -> NSError {
        return NSError(domain: ERR_noAddressErrorTitle, code: ErrorCode.noAddressError.rawValue, userInfo: nil)
    }
    
}
