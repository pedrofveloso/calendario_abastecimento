//
//  AlertController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/13/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import UIKit

class AlertController: NSObject {
    
    static let instance = AlertController()
    public override init() {}
    
    func showDefaultAlert(parent: UIViewController, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Voltar", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            parent.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlertWithCustomActions(parent: UIViewController, title: String, message: String, button1title: String? = nil, button2title:String = "Ok", button2completion: @escaping() -> Void ){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: button2title, style: .default, handler: { action in
                button2completion()
            })
        )
        if let btn1 = button1title {
            alert.addAction(
                UIAlertAction(title: btn1, style: .cancel, handler: nil)
            )
        }
        
        
        DispatchQueue.main.async {
            parent.present(alert, animated: true, completion: nil)
        }
    }
    
    func showPopover(parent: UIViewController, sender: Any, height: CGFloat, text: String,image: UIImage) -> PopoverViewController{
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Popover", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! PopoverViewController
        vc.loadData(text: text, image: image)
        let senderView = sender as! UIView
        
        let width = vc.calculateWidth(text: text, height: height)
        
        vc.preferredContentSize = CGSize(width: width, height: height)
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.permittedArrowDirections = .up
        vc.popoverPresentationController?.delegate = (parent as! UIPopoverPresentationControllerDelegate)
        
        vc.popoverPresentationController?.sourceView = senderView
        vc.popoverPresentationController?.sourceRect = CGRect(origin: CGPoint.zero, size: CGSize(width: 0, height: height))

        DispatchQueue.main.async {
            parent.present(vc, animated: true, completion: nil)
        }
        
        return vc
    }
}
