//
//  ServiceRequest.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/4/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import Alamofire


class ServiceRequest : NSObject {
    
    var url: String!
    var parameters: Dictionary<String, Any> = [:]
    var headers: Dictionary<String, String> = [:]
    var enconding: ParameterEncoding = URLEncoding(destination: .queryString)
    var method: HTTPMethod = .get
    
    let customAllowedSet =  NSCharacterSet(charactersIn: "='%/<>?@\\^`{|}, \":").inverted
    
    static func location(addressText: String, with magicKey:String) -> ServiceRequest{
        let serviceRequest = ServiceRequest()
        var paramMagicKey = ""
        
        if(magicKey.isEmpty == false){
            paramMagicKey = "&magicKey=\(magicKey)"
        }
        
        let filledUrl = String(format: URL_locationService, addressText, paramMagicKey)
        serviceRequest.url = filledUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        return serviceRequest
    }
    
    static func perimeter(x: String, y: String) -> ServiceRequest {
        let serviceRequest = ServiceRequest()
        
        var queryParameter = String(format: PARAM_queryPerimeterService, x, y)
        queryParameter = queryParameter.addingPercentEncoding(withAllowedCharacters: serviceRequest.customAllowedSet)!
        
        let outFieldsParameter = PARAM_outFieldsPerimeterService.addingPercentEncoding(withAllowedCharacters: serviceRequest.customAllowedSet)!
        
        serviceRequest.url = String(format:URL_perimeterService, queryParameter, outFieldsParameter)
        
        return serviceRequest
    }
    
    static func schedules(perimeterID: String) -> ServiceRequest {
        let serviceRequest = ServiceRequest()
        let filledUrl = String(format: URL_schedulesService, perimeterID)
        serviceRequest.url = filledUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        return serviceRequest
    }
    
    static func validDates(perimeterID:String, month: String, year:String) -> ServiceRequest {
        let serviceRequest = ServiceRequest()
        
        
        var queryParameter = String(format: PARAM_queryUrlValidDates, perimeterID, month, year)
        queryParameter = queryParameter.addingPercentEncoding(withAllowedCharacters: serviceRequest.customAllowedSet)!
        
        let suffixParameter = PARAM_suffixUrlValidDates.addingPercentEncoding(withAllowedCharacters: serviceRequest.customAllowedSet)!
        
        serviceRequest.url = String(format: URL_validDates, queryParameter, suffixParameter)
        
        return serviceRequest
    }
    
    static func maintenanceDates(perimeterID: String) -> ServiceRequest {
        let serviceRequest = ServiceRequest()
        let currentMonth = "%-\(DateHelper.getCurrentDateWith(format: "MM"))-%"
        let filledUrl = String(format: URL_maintenanceDates, perimeterID, currentMonth)
        serviceRequest.url = filledUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        return serviceRequest
    }
    
    static func suggestion(addressText: String) -> ServiceRequest{
        let serviceRequest = ServiceRequest()
        let filledUrl = String(format: URL_suggestions, addressText)
        serviceRequest.url = filledUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        return serviceRequest
    }
    
    static func address(cep: String, isRetry:Bool) -> ServiceRequest{
        let serviceRequest = ServiceRequest()
        let url = isRetry ? URL_addressRetry : URL_address
        serviceRequest.url = String(format: url, cep)
        
        return serviceRequest
    }
    
    static func reportError(type: String, info: String, content: String, cep: String) -> ServiceRequest{
        let serviceRequest = ServiceRequest()
        serviceRequest.url = URL_reportBug
        serviceRequest.enconding = URLEncoding.httpBody
        
        serviceRequest.parameters = [
            "type" : type,
            "info" : info,
            "content" : content,
            "cep" : cep
        ]
        
        serviceRequest.method = .post
        
        serviceRequest.headers = [
            "subscriptionKey" : "KI*@YuijnY6yuj",
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        
        return serviceRequest
    }
    
}
