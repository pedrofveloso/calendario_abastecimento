//
//  ServiceHandler.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import Alamofire

class ServiceHandler<T:ServiceModelProtocol>: NSObject {
    
    func callAndHandleService(serviceRequest: ServiceRequest,
                              completion: @escaping(_ error:NSError?, _ model: T?) -> Void ) -> Request? {
        
        if !Reachability.isConnectedToNetwork(){
            let error = ErrorFactory.noConncectionError()
            completion(error, nil)
            return nil
        } else{
            let request = ServiceCaller.executeRequest(serviceRequest: serviceRequest) { (error, response) in
                
                //call service
                //Handle alamofire request cancelation
                if(error?._code == -999){
                    return
                }
                
                if(error == nil){
                    let dic = response?.result.value as! Dictionary<String, Any>
                    
                    //handle republicaVirtual error to not founded cep
                    if let result = dic["resultado"]{
                        if(result as! String == "0"){
                            let error = ErrorFactory.noAddressError()
                            completion(error, nil)
                            return
                        }
                    }
                    
                    //handle viaCep error to not founded cep
                    if let _ = dic["erro"]{
                        let error = ErrorFactory.noAddressError()
                        completion(error, nil)
                    }else{
                        let model: T? = self.convertToModel(dict: dic, type: T.modelType())
                        completion(nil, model)
                    }
                }else{
                    let error = ErrorFactory.serviceError()
                    completion(error, nil)
                }
            }
            
            return request
        }
    }
    
    private func convertToModel(dict: Dictionary<String, Any>, type: String) -> T?{
        var model: T? = nil
        
        switch type{
        case LocationModel.modelType():
            model = LocationModel.convertDicToModel(dict: dict) as? T
            break
        case PerimeterModel.modelType():
            model = PerimeterModel.convertDicToModel(dict: dict) as? T
            break
        case ScheduleModel.modelType():
            model = ScheduleModel.convertDicToModel(dict: dict) as? T
            break
        case ArrayDateModel.modelType():
            model = ArrayDateModel.convertDicToModel(dict: dict) as? T
            break
        case SuggestionModel.modelType():
            model = SuggestionModel.convertDicToModel(dict: dict) as? T
            break
        case AddressModel.modelType():
            model = AddressModel.convertDicToModel(dict: dict) as? T
            break
        default:
            model = nil
        }
        
        return model
    }
}
