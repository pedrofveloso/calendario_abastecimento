//
//  ServiceModelProtocol.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

protocol ServiceModelProtocol {
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol
    static func modelType() -> String
}
