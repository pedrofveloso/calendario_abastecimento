//
//  ServiceCaller.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/4/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Alamofire
import Foundation

class ServiceCaller : NSObject {
    static let queue = DispatchQueue.global(qos: .userInteractive)
    
    static func executeRequest(serviceRequest: ServiceRequest,
                               completion:@escaping (_ error:Error?, _ response:DataResponse<Any>?)->Void) -> Request {
        
        let apiUrl = serviceRequest.url
        let enconding = serviceRequest.enconding
        let parameters = serviceRequest.parameters
        let headers =  serviceRequest.headers
        let method = serviceRequest.method
        
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 30
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForResource = 30
        let request: DataRequest = Alamofire.request(apiUrl!, method: method, parameters: parameters, encoding: enconding, headers: headers)
            .validate(statusCode: 200...999)
            .responseJSON(queue: queue) { (response:DataResponse<Any>) in
                switch(response.result){
                case .success(_):
//                    print("response: \(response.result)")
                    completion(nil, response)
                    break
                case .failure(_):
//                    print("response error: \(response.error!)")
                    completion(response.result.error, response)
                    break
                }
        }
        
        return request
    }
}
