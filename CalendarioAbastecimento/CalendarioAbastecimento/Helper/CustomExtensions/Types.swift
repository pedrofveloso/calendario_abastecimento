//
//  String.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/9/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    
    func asFormattedHour() -> String{
        
        let h = self == 24 ? 23 : self //24 should be converted to 23:59
        let m = h == 23 ? 59 : 0 ////24 should be converted to 23:59
        
        return String(format: "%02d:%02d", h, m)
    }
}


extension String {
    
    func capitalizedFirstLetter() -> String{
        let first = self.prefix(1).uppercased()
        let others = self.dropFirst()
        
        return first + others
    }
    
    func convertToHour(limit: Int) -> String{
        let prefix = self.prefix(limit)
        if let int = Int(prefix){
            return int.asFormattedHour()
        }
        return "00:00"
    }
}

extension UIColor{

    public class var maintanence:UIColor{
       return UIColor(red: 208/255, green: 93/255, blue: 70/255, alpha: 1.0)
    }
    
    public class var blue_schedule: UIColor {
        return UIColor(red: 0.467, green: 0.686, blue: 0.816, alpha: 1.0)
    }
    
    public class var spacialGray: UIColor {
        return UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0)
    }
}

extension UITextField{
    
    func addInvalidStyle(){
        self.textColor = UIColor.maintanence
    }
    
    func resetStyle(){
        self.textColor = UIColor.spacialGray
    }
    
    func isValidToLimit(limit: Int, string: String, range: NSRange) -> Bool {
        guard let text = self.text else { return true }
        let newLength = text.count + string.count - range.length
        
        return newLength <= limit
    }
}

extension URL {
    
    func open(){
        if(UIApplication.shared.canOpenURL(self)){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self)
            }
        }
    }
}
