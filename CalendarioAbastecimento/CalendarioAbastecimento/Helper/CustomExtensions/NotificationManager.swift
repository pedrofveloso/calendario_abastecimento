//
//  NotificationManager.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/15/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import UserNotifications
import UserNotificationsUI

class NotificationManager : NSObject {
    
    static func scheduleNotification(to calendar: CalendarModel){
        
        for mapKey in calendar.map.keys {
            let arrDateModel = calendar.map[mapKey]
            if let dates = arrDateModel?.dates {
                for date in dates {
                    mountNotification(dateModel: date, cep: calendar.address.cep)
                }
            }
        }
    }
    
    
    static func mountNotification(dateModel: DateModel, cep:String){
        let date = DateHelper.getDateNotificationFrom(timeInterval: dateModel.timeInterval, initialTime: dateModel.initialTime)
        let notification = UILocalNotification()
        notification.fireDate = date
        notification.alertTitle = "Data Agua PE Avisa:"
        notification.alertBody = dateModel.isMaintenance ? String(format:NOTIFY_scheduled, cep) : String(format:NOTIFY_scheduled, cep)
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.category = "\(cep)"
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    static func removeNotifications(to calendarModel: CalendarModel){
        var newNotifications:[UILocalNotification] = []
        if let notifications = UIApplication.shared.scheduledLocalNotifications {
            for n in notifications {
                if(n.category != calendarModel.address.cep){
                    newNotifications.append(n)
                }
            }
        }
        
        UIApplication.shared.scheduledLocalNotifications = newNotifications
    }
    
}
