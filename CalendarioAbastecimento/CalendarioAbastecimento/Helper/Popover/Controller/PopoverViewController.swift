//
//  PopoverViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/14/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import UIKit

class PopoverViewController: UIViewController{
    
    let MARGIN:CGFloat = 15.0
    let IMAGESIZE: CGFloat = 20.0
    
    @IBOutlet weak var lbText: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var text: String!
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.image = self.image
        self.lbText.text = text
    }
    
    func loadData(text: String, image:UIImage? = nil){
        self.image = image
        self.text = text
    }

    func calculateWidth(text: String, height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: UIFont(name: FONT_trebuchet, size: 14 )!], context: nil)
        
        return ceil(boundingBox.width) + MARGIN*3 + IMAGESIZE
    }
}
