//
//  DataManager.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/13/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation


class DataManager {
    
    static func getFavorites() -> [AddressModel]{
        if let arrDictAddress = UserDefaults.standard.array(forKey: UDK_Ceps) {
            var arrAdressModel: [AddressModel] = []
            
            for dict  in arrDictAddress as! [Dictionary<String, Any>] {
                let model = AddressModel.convertDicToModel(dict: dict) as! AddressModel
                arrAdressModel.append(model)
            }
            
            return arrAdressModel
        }
        
        return []
    }
    
    static func setFavorites(arrAddressModel: [AddressModel]){
        
        var arrDictAddress: [Dictionary<String,String>] = []
        
        for model in arrAddressModel {
            arrDictAddress.append(AddressModel.convertModelToDict(model: model))
        }
        
        UserDefaults.standard.set(arrDictAddress, forKey: UDK_Ceps)
        UserDefaults.standard.synchronize()
    }
    
    static func addToFavorites(address: AddressModel){
        var arrAddressModel = getFavorites()
        arrAddressModel.append(address)
        setFavorites(arrAddressModel: arrAddressModel)
    }
    
    static func removeFromFavorites(address: AddressModel){
        var arrAddressModel = getFavorites()

        if let addressIndex = arrAddressModel.index(where: { (dbAddress) -> Bool in
            address.cep == dbAddress.cep
        }){
            arrAddressModel.remove(at: addressIndex)
        }
        
        setFavorites(arrAddressModel: arrAddressModel)
    }
    
}
