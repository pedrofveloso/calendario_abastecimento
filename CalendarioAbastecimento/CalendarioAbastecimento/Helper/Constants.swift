//
//  Constants.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/3/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

//MARK:- UserDefault keys
let UDK_Ceps = "udkCeps"

//MARK:- Cell Identifiers
let CELLID_favoriteCep = "favoriteCepCellIdentifier"
let CELLID_calendar = "calendarCellIdentifier"

//MARK:- Error Mensages
let ERR_noInternetConnectionMessage = "Sem conexão com Internet"
let ERR_serviceErrorMessage = "O servidor se comportou de forma inesperada. Por favor tente novamente"
let ERR_noAddressErrorTitle = "CEP não encontrado"
let ERR_noAddressErrorMessage = "Certifique-se que digitou o seu CEP corretamente e tente novamente"

//MARK: - Font name
let FONT_trebuchetBold = "TrebuchetMS-Bold"
let FONT_trebuchet = "TrebuchetMS"

//MARK:- Model Types
let MT_location = "locationModel"
let MT_perimeter = "perimeterModel"
let MT_date = "dateModel"
let MT_schedule = "scheduleModel"
let MT_suggestion = "suggestionModel"
let MT_address = "addressModel"

//MARK:- Notifications
let NT_onReceivedCalendar = Notification.Name("onReceivedCalendar")
let NT_onReceivedSuccess = Notification.Name("onReceiveSuccess")
let NT_onReceivedError = Notification.Name("onReceivedError")
let NT_onReceivedReportSuccess = Notification.Name("onReceivedReportSuccess")
let NT_onReceivedReportError = Notification.Name("onReceivedReportError")

//MARK:- General Texts
let ALERT_titleAddToFavorites = "Deseja adicionar a sua lista de endereços favoritos?"
let ALERT_messageAddToFavorites = "Ao favoritar um endereço você receberá notificações sobre o abastecimento."
let ALERT_titleRemoveFavorites = "Deseja remover este endereço da sua lista favoritos?"
let ALERT_messageRemoveFavorites = "Ao remover você não será mais notificado sobre o abastecimento desse CEP."
let ALERT_titleReportBug = "Obrigado!"
let ALERT_messageReportBug = "Seu relatório será analisado por nós! Muito obrigado por nos ajudar a melhorar ainda mais o nosso App!"
let ALERT_titleNoDates = "Sem resultados"
let ALERT_messageNoDates = "O serviço não retornou datas de abastecimento para a sua região. Tente novamente mais tarde, ou entre em contato com a companhia de saneamento."
let ALERT_titleWarningReportBug = "Ops!"
let ALERT_messageWarningReportBug = "Verifique os dados inseridos. Algo parece não estar certo."
let TEXT_scheduledSupply = "Abastecimento agendado"
let TEXT_scheduledMaintenance = "Manutenção agendada"
let TEXT_loading_validatingCEP = "Validando CEP..."
let TEXT_loading_searchingAddress = "Buscando endereço..."
let TEXT_loading_localizingReservatory = "Localizando reservatório..."
let TEXT_loading_searchingSchedule = "Consultando horários..."
let TEXT_loading_searchingDates = "Consultando datas..."
let TEXT_loading_creatingCalendar = "Montando calendário..."
let TEXT_popover_favorites =  "Adicione este CEP aos favoritos"
let TEXT_popover_invalidCharCEP = "CEP deve conter apenas números"
let TEXT_popover_invalidLengthCEP = "CEP deve conter 8 dígitos"
let NOTIFY_scheduled = "O CEP %@ possui um abastecimento agendado para hoje! Economize Água, o planeta agradece! 😊"
let NOTIFY_maintenance = "Fique esperto! O CEP %@ será afetado por uma manutenção agendada para hoje! Economize Água, o planeta agradece! 😊"
let BT_email = "Contate-nos via: kongoapps@gmail.com"
let TEXT_copied = "Copiado!"
//MARK:- General Values
let HEIGHT_txCep = 50
let HEIGHT_btFav = 30
let MIN_PHONE_SIZE = 10

//MARK:- URLs
let URL_address = "http://viacep.com.br/ws/%@/json/"
let URL_addressRetry = "http://cep.republicavirtual.com.br/web_cep.php?cep=%@&formato=json"

let URL_suggestions = "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/suggest?f=json&text=%@&maxSuggestions=6&location={\"x\":-3883504.536351947,\"y\":-887233.9557538835,\"spatialReference\":{\"wkid\":102100,\"latestWkid\":3857}}&distance=50000"

let URL_locationService = "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=%@, BRA&f=json&outSR={\"wkid\":102100,\"latestWkid\":3857}&outFields=Addr_type,Match_addr,StAddr,City&distance=50000&location={\"x\":-3889022.2024229807,\"y\":-892676.8633280713,\"spatialReference\":{\"wkid\":102100,\"latestWkid\":3857}}&maxLocations=6"

let URL_perimeterService = "https://geo.compesa.com.br:6443/arcgis/rest/services/Calendario/Calendario/MapServer/0/query?f=json&returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry=%@&geometryType=esriGeometryPoint&inSR=102100&outFields=%@&outSR=102100"

let PARAM_queryPerimeterService = "{\"x\":%@,\"y\":%@,\"spatialReference\":{\"wkid\":102100,\"latestWkid\":3857}}"
let PARAM_outFieldsPerimeterService = "OBJECTID,NOMECALEND,NOMABAST,ID"

let URL_schedulesService = "http://geo.compesa.com.br/arcgis/rest/services/Calendario/Calendario/MapServer/4/query?f=json&where=ID='%@'&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=HORARIO"

let URL_validDates = "https://geo.compesa.com.br:6443/arcgis/rest/services/Calendario/Calendario/MapServer/5/query?f=json&where=%@&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=%@"

let PARAM_queryUrlValidDates = "ID='%@' AND DATEPART(MONTH,Inicio)='%@' AND DATEPART(YEAR,Inicio)='%@'"
let PARAM_suffixUrlValidDates = "Inicio,Termino,colapso"

let URL_maintenanceDates = "http://geo.compesa.com.br/arcgis/rest/services/Calendario/Calendario/MapServer/2/query?f=json&where=ID_AREA_ABASTECIMENTO='%@' AND INICIO_PREVISTO LIKE '%@'&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=INICIO_PREVISTO,TERMINO_PREVISTO,DESCRICAO_SERVICO"

let URL_reportBug = "https://sheltered-river-40356.herokuapp.com/dataaguape/reportbug"

//MARK:- Regular Expressions
let REGEX_email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

