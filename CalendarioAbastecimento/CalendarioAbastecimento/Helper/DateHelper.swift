//
//  DateHelper.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/8/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class DateHelper {
    
    static var calendar: Calendar  = getCalendar()
    
    static func isTimeIntervalFromCurrentMonth(timeInterval: TimeInterval) -> Bool{
        let comp: DateComponents = calendar.dateComponents([.year, .month], from: Date())
        
        let startOfMonth = calendar.date(from: comp)!
        return timeInterval/1000 >= startOfMonth.timeIntervalSince1970
    }
    
    static func isEqualDates(timeInterval1: TimeInterval, timeInterval2: TimeInterval) -> Bool {
        var compDate1 = calendar.dateComponents([.year, .month, .day], from: Date(timeIntervalSince1970: timeInterval1/1000))
        var compDate2 = calendar.dateComponents([.year, .month, .day], from: Date(timeIntervalSince1970: timeInterval2/1000))
        
        compDate1.timeZone = TimeZone(secondsFromGMT: 0)
        compDate2.timeZone = TimeZone(secondsFromGMT: 0)
        
        let date1 = Calendar.current.date(from: compDate1)
        let date2 = Calendar.current.date(from: compDate2)
        
        return date1?.compare(date2!) == .orderedSame
    }
    
    static func getTimesFrom(schedule: String) -> (initial: String?, final: String?, isIntersection:Bool){
        let text: NSString = schedule as NSString
        var arr:[String] = []
        
        let rx = try! NSRegularExpression(pattern: "([0-9]+ h|[0-9]+:[0-9]+|[0-9]+ à)", options: [])
        let range = NSMakeRange(0, text.length)
        let m = rx.matches(in: text.description, options: [], range: range)
        for matchObject in m {
            let match = matchObject as NSTextCheckingResult
            let range = match.range(at: 1)
            
            var strValue = text.substring(with: range)
            strValue = strValue.convertToHour(limit: 2)

            arr.append(strValue)
        }
        
        var initial: String? = nil
        var final: String? = nil
        
        if(arr.isEmpty == false){
            initial = arr[0]
            final = arr.count == 1 ? nil : arr[1]
        }

        return(initial, final, initial == final)
    }
    
    static func diffBetweenTimeIntervals(minorTimeInterval: TimeInterval, biggerTimeInterval: TimeInterval) -> Int{
        let biggerDate = Date(timeIntervalSince1970: biggerTimeInterval/1000)
        let minorDate = Date(timeIntervalSince1970: minorTimeInterval/1000)
        
        var compBiggerDate = calendar.dateComponents([.day], from: biggerDate)
        var compMinorDate = calendar.dateComponents([.day], from: minorDate)
        
        compBiggerDate.timeZone = TimeZone(secondsFromGMT: 0)
        compMinorDate.timeZone = TimeZone(secondsFromGMT: 0)
    
        return compBiggerDate.day! - compMinorDate.day! + 1
    }
    
    static func getTimeFrom(timeInterval: TimeInterval) -> Int{
        var comp = calendar.dateComponents([.hour], from: Date(timeIntervalSince1970: timeInterval/1000))
        comp.timeZone = TimeZone(abbreviation: "GMT")
        return comp.hour!
    }
    
    static func addToTimeInterval(days: Int, timeInterval: TimeInterval) -> TimeInterval{
        var date = Date(timeIntervalSince1970: timeInterval/1000)
        date = calendar.date(byAdding: .day, value: days, to: date)!
        
        return date.timeIntervalSince1970 * 1000
    }
    
    static func getCalendar(gmtHour: Int = 0) -> Calendar{
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: (gmtHour * 60 * 60))!
        return calendar
    }
    
    static func getDateNotificationFrom(timeInterval: TimeInterval, initialTime: String) -> Date {
        let date = Date(timeIntervalSince1970: timeInterval/1000)
        var dc = self.getCalendar(gmtHour: -3).dateComponents([.day, .month, .year, .hour, .minute], from: date)
        dc.hour = Int(initialTime.prefix(2))
        dc.minute = Int(initialTime.suffix(2))
        return self.getCalendar(gmtHour: -3).date(from: dc)!
    }
    
    static func getFormattedDateFrom(timeInterval:TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeInterval/1000)
        let df = DateFormatter()
        df.dateFormat = "E, dd 'de' MMMM"
        df.locale = Locale(identifier: "pt_BR")
        
        return df.string(from: date)
    }
    
    static func getMonthDay(from timeInterval: TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeInterval/1000)
        let df = DateFormatter()
        df.dateFormat = "MMdd"
        
        
        return df.string(from: date)
    }
    
    static func getCurrentDateWith(format:String) -> String{
        let df = DateFormatter()
        df.dateFormat = format
        df.locale = Locale(identifier: "pt_BR")
        return df.string(from: Date())
    }
}
