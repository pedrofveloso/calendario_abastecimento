//
//  CalendarTableViewCell.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/4/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit

class CalendarTableViewCell: UITableViewCell {
//    MARK:- IBOutlets
    @IBOutlet weak var lbInitialTime: UILabel!
    @IBOutlet weak var lbFinalTime: UILabel!
    @IBOutlet weak var viewStatusColor: UIView!
    @IBOutlet weak var lbDetail: UILabel!
    @IBOutlet weak var imvStatus: UIImageView!
    
//    MARK:- Proprerties
    var model: DateModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadData() {
        DispatchQueue.main.async {
            self.lbInitialTime.text = self.model.initialTime
            self.lbFinalTime.text = self.model.finalTime
            
            let color = self.model.isMaintenance ? UIColor.maintanence : UIColor.blue_schedule
            self.viewStatusColor.backgroundColor = color
            
            let detailText = self.model.isMaintenance ? TEXT_scheduledMaintenance : TEXT_scheduledSupply
            self.lbDetail.text = detailText
            
            self.imvStatus.image = self.model.isMaintenance ? #imageLiteral(resourceName: "maintenanceIcon") : #imageLiteral(resourceName: "waterIcon")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
