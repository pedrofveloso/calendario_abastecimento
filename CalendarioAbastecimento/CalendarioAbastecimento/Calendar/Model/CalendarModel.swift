//
//  CalendarModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class CalendarModel {
    
    var map: Dictionary<String, ArrayDateModel> = [:]
    var address: AddressModel = AddressModel()
    
    func addMaintenanceDates(maintenanceDates: ArrayDateModel){
        
        var key = ""
        
        for maintenanceDate in maintenanceDates.dates{
            var toAdd = true
            key = DateHelper.getMonthDay(from: maintenanceDate.timeInterval)

            if let calendar = self.map[key]{
                for date in calendar.dates{
                    if(DateHelper.isEqualDates(timeInterval1: date.timeInterval, timeInterval2: maintenanceDate.timeInterval) && date.isMaintenance){
                        date.isMaintenance = true
                        date.timeInterval = maintenanceDate.timeInterval
                        date.initialTime = maintenanceDate.initialTime
                        date.finalTime = maintenanceDate.finalTime
                        toAdd = false
                        break
                    }
                }
                
                if(toAdd){
                    self.map[key]?.dates.append(maintenanceDate)
                }
                
            } else{
                self.map[key] = ArrayDateModel()
                self.map[key]?.dates.append(maintenanceDate)
            }
        }
    }
    
    func mapDates(arrayDataModel: ArrayDateModel){
        
        var lastKey = ""
        
        for date in arrayDataModel.dates {
            lastKey = DateHelper.getMonthDay(from: date.timeInterval)
            
            if let mapForKey = self.map[lastKey]{
                mapForKey.dates = self.addDateToKey(arrayDataModel: mapForKey.dates, date: date)
                
            }else{
                self.map[lastKey] = ArrayDateModel()
                self.map[lastKey]?.dates = self.addDateToKey(arrayDataModel: (self.map[lastKey]?.dates)!, date: date)
            }
        }
        
        
    }
    //handle problem of duplicated dates
    private func addDateToKey(arrayDataModel: [DateModel], date: DateModel) -> [DateModel]{
        var arrDateModel = arrayDataModel
        
        let alreadyAdded = arrDateModel.contains(where: { (dm) -> Bool in
            dm.timeInterval == date.timeInterval
        })
        
        if(alreadyAdded == false) {
            arrDateModel.append(date)
        }
        
        return arrDateModel
    }
}
