//
//  CalendarViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/10/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate{
//    MARK:- IBOutlets
    @IBOutlet weak var tbvCalendar: UITableView!
    
    @IBOutlet weak var btFavorite: UIButton!
    @IBOutlet weak var lbAddress: UILabel!

//    MARK:- Properties
    var calendarModel: CalendarModel!
    var currentMonth: String!
    var keys:[String] = []
    var isFavorite: Bool = false
    
    var delegate: FavoritesDelegate!
    
//    MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = DateHelper.getCurrentDateWith(format: "MMMM").uppercased()
        
        self.isFavorite = self.checkIsFavorite()
        self.updateBtFavorite(isFavorite: self.isFavorite)
        
        self.view.backgroundColor = UIColor.blue_schedule;
        
        self.setupAddress()
        self.setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.isFavorite == false){
            let popover = AlertController.instance.showPopover(parent: self, sender: self.btFavorite, height: CGFloat(HEIGHT_btFav), text: TEXT_popover_favorites,image: UIImage())
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                popover.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK:- Table View Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.keys = Array(self.calendarModel.map.keys).sorted()
        return self.calendarModel.map.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let mapForKey = self.calendarModel.map[self.keys[section]]{
            return mapForKey.dates.count
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELLID_calendar, for: indexPath) as! CalendarTableViewCell
        
        let key = self.keys[indexPath.section]
        
        if let mapForKey = self.calendarModel.map[key]{
            cell.model = mapForKey.dates[indexPath.row]
            cell.loadData()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let key = self.keys[section]
        let title = DateHelper.getFormattedDateFrom(timeInterval: (self.calendarModel.map[key]?.dates.first?.timeInterval)!)
        
        return title.capitalizedFirstLetter()
    }
    
//    MARK:- Setup Methods
    private func setupAddress(){
        let cep = self.calendarModel.address.cep
        let street = self.calendarModel.address.street
        let city = self.calendarModel.address.city
        
        var address = cep
        
        if(street.isEmpty == false){
            address += " - \(street)"
        }
        
        if(city.isEmpty == false){
            address += " / \(city)"
        }
        
        self.lbAddress.text = address
    }
    
    private func setupTableView(){
        self.tbvCalendar.delegate = self
        self.tbvCalendar.dataSource = self
        
        self.tbvCalendar.register(UINib(nibName: "CalendarTableViewCell", bundle: nil), forCellReuseIdentifier: CELLID_calendar)
    }
    
    @IBAction func onTapFavorite() {
        DispatchQueue.main.async {
            if(self.isFavorite){
                AlertController.instance.showAlertWithCustomActions(parent: self, title: ALERT_titleRemoveFavorites, message: ALERT_messageRemoveFavorites, button1title: "Cancelar", button2completion: {
                    self.updateBtFavorite(isFavorite: false)
                    DataManager.removeFromFavorites(address: self.calendarModel.address)
                    NotificationManager.removeNotifications(to: self.calendarModel)
                })
            }else{
                
                AlertController.instance.showAlertWithCustomActions(parent: self, title: ALERT_titleAddToFavorites, message: ALERT_messageAddToFavorites, button1title: "Cancelar", button2completion: {
                    self.updateBtFavorite(isFavorite: true)
                    DataManager.addToFavorites(address: self.calendarModel.address)
                    NotificationManager.scheduleNotification(to: self.calendarModel)
                })
            }
            
            self.isFavorite = !self.isFavorite
        }
    }
    
    private func updateBtFavorite(isFavorite: Bool){
        DispatchQueue.main.async {
            let image = isFavorite ? #imageLiteral(resourceName: "favIcon") : #imageLiteral(resourceName: "favIconDisabled")
            self.btFavorite.setImage(image, for: .normal)
        }
    }
    
    private func checkIsFavorite() -> Bool {
        let arrAddressModel = DataManager.getFavorites()
        
        return arrAddressModel.contains(where: { (address) -> Bool in
            address.cep == self.calendarModel.address.cep
        })
    }
    
    //    MARK:- UIPopoverPresentationControllerDelegate Methods    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
