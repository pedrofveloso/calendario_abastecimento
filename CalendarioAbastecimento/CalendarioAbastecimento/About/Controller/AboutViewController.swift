//
//  AboutViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/16/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit
import MessageUI

class AboutViewController: UIViewController, MFMailComposeViewControllerDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var btLinkedinBia: UIButton!
    @IBOutlet weak var btLinkedinPedro: UIButton!
    @IBOutlet weak var btEmail: UIButton!
    
    var popover: PopoverViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.blue_schedule
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    MARK:- IBActions
    
    
    @IBAction func onClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtLinkedin(_ sender: UIButton) {
        
        if let linkedinUrl = sender.titleLabel?.text{
            guard let url = URL(string: "http://\(linkedinUrl)") else {
                return //be safe
            }
            
            url.open()
        }
        
        
    }
    
    
    @IBAction func onBtEmail() {
        if !MFMailComposeViewController.canSendMail() {
            
            self.btEmail.setTitle(TEXT_copied , for: .normal)
            self.btEmail.isEnabled = false
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                self.btEmail.setTitle(BT_email, for: .normal)
                self.btEmail.isEnabled = true
            })
            
            UIPasteboard.general.string = "kongoapps@gmail.com"
            
            return
        }

            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self

            // Configure the fields of the interface.
            composeVC.setToRecipients(["kongoapps@gmail.com"])
            composeVC.setSubject("[DATA AGUA PE] Olá!")
            composeVC.setMessageBody("", isHTML: false)

            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
    }
    
    @IBAction func onTermsAndPolicy() {
        guard let url = URL(string: "https://sites.google.com/view/dataaguapetermos/home?authuser=1") else {
            return //be safe
        }
        
        url.open()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if(error == nil){
            AlertController.instance.showAlertWithCustomActions(parent: self, title: "Email enviado com sucesso!", message: "Obrigado pelo seu contato :)", button2completion: {
                self.dismiss(animated: true, completion: nil)
            })
        }else{
            AlertController.instance.showDefaultAlert(parent: self, title: "Oops!", message: "Seu email não foi enviado :( Tente novamente!")
        }
    }
    
    //    MARK:- UIPopoverPresentationControllerDelegate Methods
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
}
