//
//  ReportErrorBusiness.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/20/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class ReportErrorBusiness: NSObject{
    
    static let instance = ReportErrorBusiness()
    
    var type: Int?
    var info: String?
    var content: String?
    var alertObject: (title:String, message:String)?
    var isLoading: Bool = false
    
    private override init(){}
    
    func isInfo(info: String, validWith type: Int) -> Bool {
        if(type == 2){
            return NSPredicate(format: "SELF MATCHES %@", REGEX_email).evaluate(with: info)
            
        }else{
            if(info.count == MIN_PHONE_SIZE + type){
                return true
            }else{
                return false
            }
        }
    }
    
    func sendBugReport(type: String, info: String, content: String) {
        let serviceRequest = ServiceRequest.reportError(type: type, info: info, content: content, cep: "")
        
        _ = ReportErrorService.sendBugReport(serviceRequest: serviceRequest) { (error) in
    
            self.isLoading = false
            
            if(error == nil){
                self.alertObject = (ALERT_titleReportBug, ALERT_messageReportBug)
                NotificationCenter.default.post(name: NT_onReceivedReportSuccess, object: self.alertObject)
            }else{
                self.alertObject = ("Ops!", error?.domain) as? (title: String, message: String)
                NotificationCenter.default.post(name: NT_onReceivedReportError, object: self.alertObject)
            }
        }
    }
    
    func clearAttributes() {
        self.type = nil
        self.info = nil
        self.content = nil
        self.alertObject = nil
        self.isLoading = false
    }
    
}
