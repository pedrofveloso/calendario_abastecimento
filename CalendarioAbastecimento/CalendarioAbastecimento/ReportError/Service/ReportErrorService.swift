//
//  ReportErrorService.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/20/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import Alamofire

class ReportErrorService: NSObject {
    
    static func sendBugReport(serviceRequest: ServiceRequest, completion: @escaping(_ error: NSError?) -> Void) -> Request? {
        
        if !Reachability.isConnectedToNetwork(){
            let error = ErrorFactory.noConncectionError()
            completion(error)
            return nil
            
        }else {
            let request = ServiceCaller.executeRequest(serviceRequest: serviceRequest) { (error, response) in
                if(error?._code == -999){
                    return
                }
                
                if(error == nil){
                    let dic = response?.result.value as! Dictionary<String, Any>
                    if let err = dic["err"]{
                        
                        if(err is NSNull){
                            completion(nil)
                        }else{
                            completion(ErrorFactory.serviceError())
                        }
                    }else{
                        completion(ErrorFactory.serviceError())
                    }
                    
                }else{
                    let error = ErrorFactory.serviceError()
                    completion(error)
                }
            }
            return request
        }
    }
}
