//
//  ReportErrorViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/19/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit

class ReportErrorViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    //MARK: - IBOutlets
    @IBOutlet weak var vwStatusBar: UIView!
    @IBOutlet weak var vwTopBar: UIView!
    @IBOutlet weak var scType: UISegmentedControl!
    @IBOutlet weak var txfInfo: UITextField!
    @IBOutlet weak var txvContent: UITextView!
    @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var aiLoading: UIActivityIndicatorView!
    
    var currentNotification:Notification?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupComponents()
        self.setupNotification()
        
        self.activateBtSend()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK: - Notification
    private func setupNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(showAlertSuccess), name: NT_onReceivedReportSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showAlertError), name: NT_onReceivedReportError, object: nil)
        
        // keyboard notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    

    //MARK: - IBActions
    @IBAction func onClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSetType(_ sender: UISegmentedControl) {
        
        self.txfInfo.resignFirstResponder()
        self.txfInfo.text = ""
        self.txfInfo.placeholder = "\(sender.titleForSegment(at: sender.selectedSegmentIndex)!)"
        
        if(sender.selectedSegmentIndex == 2){
            self.txfInfo.keyboardType = .emailAddress
            
        }
        else{
            self.txfInfo.keyboardType = .numberPad
            self.txfInfo.placeholder = "\(sender.titleForSegment(at: sender.selectedSegmentIndex)!) com ddd"
        }
        
        self.activateBtSend()
    }
    
    @IBAction func onSend() {
        
        if(shouldSendInfo(consideringIndex: self.scType.selectedSegmentIndex, and: self.txvContent.text.isEmpty)){
            self.setLoadingAnimation(to: true)
            ReportErrorBusiness.instance.sendBugReport(type: self.scType.titleForSegment(at: self.scType.selectedSegmentIndex)!, info: self.txfInfo.text!, content: self.txvContent.text!)
            
        }else{
            AlertController.instance.showDefaultAlert(parent: self, title: ALERT_titleWarningReportBug, message: ALERT_messageWarningReportBug)
        }

    }
    
//    MARK:- UITextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let limit = self.scType.selectedSegmentIndex == 2 ? 64 : (self.scType.selectedSegmentIndex + MIN_PHONE_SIZE)
        
        return textField.isValidToLimit(limit: limit, string: string, range: range)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let notification = self.currentNotification {
            self.upKeyboard(notification: notification)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activateBtSend()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if(self.txfInfo.isFirstResponder){
            self.txfInfo.resignFirstResponder()
        }
        if(self.txvContent.isFirstResponder){
            self.txvContent.resignFirstResponder()
        }
    }
    
//    MARK:- UITextView Delegate
    func textViewDidEndEditing(_ textView: UITextView) {
        self.activateBtSend()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.activateBtSend()
    }
    
//    MARK:- Setup Methods
    private func setupComponents() {
        DispatchQueue.main.async {
            self.scType.tintColor = UIColor.blue_schedule
            self.scType.layer.cornerRadius = 12
            self.scType.layer.borderWidth = 1
            self.scType.layer.borderColor = self.scType.tintColor.cgColor
            self.scType.layer.masksToBounds = true
            
            self.btSend.setTitleColor(UIColor.white, for: .normal)
            self.btSend.setTitleColor(UIColor.white, for: .disabled)
            
            self.btSend.layer.borderWidth = 1
            self.btSend.layer.cornerRadius = self.btSend.frame.height/2
        }
        
        self.vwStatusBar.backgroundColor = UIColor.blue_schedule
        self.vwTopBar.backgroundColor = UIColor.blue_schedule
        
        self.txfInfo.delegate = self
        self.txvContent.delegate = self
        
        
        if(ReportErrorBusiness.instance.alertObject != nil){
            self.setLoadingAnimation(to: false)
            AlertController.instance.showDefaultAlert(parent: self, title: (ReportErrorBusiness.instance.alertObject?.title)!, message: (ReportErrorBusiness.instance.alertObject?.message)!)
            
            ReportErrorBusiness.instance.clearAttributes()
        }else{
            if(ReportErrorBusiness.instance.isLoading){
                self.setLoadingAnimation(to: true)
                self.fillFields()
            }else{
                self.setLoadingAnimation(to: false)
            }
        }
    }
    
    //    MARK:- Keyboard Methods
    @objc private func keyboardWillShow(notification: Notification){
        self.currentNotification = notification
        self.upKeyboard(notification: self.currentNotification!)
    }
    
    @objc private func keyboardWillHide(notification: Notification){
        self.view.frame = CGRect(x: self.view.bounds.origin.x,
                                 y: 0,
                                 width: self.view.frame.width,
                                 height: self.view.frame.height)
    }
    
    
//    MARK:- Methods
    private func upKeyboard(notification: Notification){
        var info = notification.userInfo!
        
        if(self.view.frame.origin.y >= 0 && self.txvContent.isFirstResponder){
            if let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                
                let finalPoint = CGPoint(x: self.txvContent.frame.origin.x, y: self.txvContent.frame.origin.y + self.txvContent.frame.height)
                
                if(keyboardRect.contains(finalPoint)){
                    let delta = finalPoint.y - (keyboardRect.origin.y)
                    self.view.frame = CGRect(x: self.view.bounds.origin.x,
                                             y: self.view.frame.origin.y - delta,
                                             width: self.view.frame.width,
                                             height: self.view.frame.height)
                }
            }
        }
    }
    
    
    private func shouldSendInfo(consideringIndex index: Int, and isContentEmpty: Bool) -> Bool {
        return ReportErrorBusiness.instance.isInfo(info: self.txfInfo.text!, validWith: index) && (isContentEmpty == false)
    }
    
    private func activateBtSend(){
        self.btSend.isEnabled = self.shouldSendInfo(consideringIndex: self.scType.selectedSegmentIndex, and: self.txvContent.text.isEmpty)
        
        
        if(self.btSend.isEnabled){
            self.btSend.layer.borderColor = UIColor.blue_schedule.cgColor
            self.btSend.backgroundColor = UIColor.blue_schedule
        }else{
            self.btSend.layer.borderColor = UIColor.lightGray.cgColor
            self.btSend.backgroundColor = UIColor.lightGray
        }
    }
    
    @objc private func showAlertSuccess(notification: Notification){
        let alertContent = notification.object as! (title: String, message: String)
        AlertController.instance.showAlertWithCustomActions(parent: self, title: alertContent.title, message: alertContent.message) {
            self.dismiss(animated: true, completion: nil)
            ReportErrorBusiness.instance.clearAttributes()
        }
    }
    
    @objc private func showAlertError(notification: Notification){
        let alertContent = notification.object as! (title: String, message: String)
        AlertController.instance.showDefaultAlert(parent: self, title: alertContent.title, message: alertContent.message)
        
        self.setLoadingAnimation(to: false)
        ReportErrorBusiness.instance.clearAttributes()
    }
    
    private func fillFields(){
        DispatchQueue.main.async {
            if let scTypeIndex = ReportErrorBusiness.instance.type {
                self.scType.selectedSegmentIndex = scTypeIndex
            }
            
            if let info = ReportErrorBusiness.instance.info {
                self.txfInfo.text = info
            }
            
            if let content = ReportErrorBusiness.instance.content {
                self.txvContent.text = content
            }
        }
    }
    
    private func setLoadingAnimation(to value: Bool){
        DispatchQueue.main.async {
            value == true ? self.aiLoading.startAnimating() : self.aiLoading.stopAnimating()
            self.btSend.isHidden = value
            
        }
    }
}
