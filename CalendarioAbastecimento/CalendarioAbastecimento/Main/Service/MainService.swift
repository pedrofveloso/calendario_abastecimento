//
//  MainService.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import Alamofire

class MainService<T:ServiceModelProtocol>: NSObject{
    
    func callService(serviceRequest: ServiceRequest, completion: @escaping(Error?, T?) -> Void){
        
        _ = ServiceHandler<T>().callAndHandleService(serviceRequest: serviceRequest, completion: completion)
    }
}
