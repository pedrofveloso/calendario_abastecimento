//
//  ArrayDateModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class ArrayDateModel : ServiceModelProtocol {
    var dates: [DateModel] = []
    
    static func calculateIntersections(initialTime: TimeInterval, finalTime:TimeInterval, isMaintenance:Bool = true) -> [DateModel]{
        let days = DateHelper.diffBetweenTimeIntervals(minorTimeInterval: initialTime, biggerTimeInterval: finalTime)
        
        var dates:[DateModel] = []
        
        for day in 0..<days{
            let date = DateModel()
            date.timeInterval = DateHelper.addToTimeInterval(days: day, timeInterval: initialTime)
            date.isMaintenance = isMaintenance
            
            if( day == 0 ){
                date.initialTime = DateHelper.getTimeFrom(timeInterval: initialTime).asFormattedHour()
            }else if( day == days-1 ){
                date.finalTime = DateHelper.getTimeFrom(timeInterval: finalTime).asFormattedHour()
            }
            
            dates.append(date)
        }
        
        return dates
    }
    
    func addSchedulesToDates(scheduleModel:ScheduleModel){
        var tempDates:[DateModel] = []

        for date in self.dates{
            let infoTuple = DateHelper.getTimesFrom(schedule: scheduleModel.scheduleInfo)
            var finalTime:String? = infoTuple.final

            if(infoTuple.isIntersection){
                let newDate = DateModel()
                newDate.timeInterval = DateHelper.addToTimeInterval(days: 1, timeInterval: date.timeInterval)
                newDate.isMaintenance = date.isMaintenance
                newDate.addScheduleInfo(initialTime: nil, finalTime: infoTuple.final)
                tempDates.append(newDate)

                finalTime = nil // first date must have finaltime equal to 23:59
            }

            date.addScheduleInfo(initialTime: infoTuple.initial, finalTime: finalTime)
        }

        self.dates.append(contentsOf: tempDates)
    }
    
    
    //  MARK:- ServiceModelProtocol Methods
    
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol {
        let arrDateModel : ArrayDateModel = ArrayDateModel()
        var arrTimeIntervals: [TimeInterval] = []
        
        let features: [Dictionary<String, Any>] = dict["features"] as! [Dictionary<String, Any>]
        
        for el in features{
            if let attr = el["attributes"] as? Dictionary<String, Any>{
                
                if let start = attr["Inicio"], let end = attr["Termino"]{
                    let startTimeInterval = start as! TimeInterval
                    let endTimeInterval = end as! TimeInterval
                    
                    if(DateHelper.isTimeIntervalFromCurrentMonth(timeInterval: startTimeInterval) && DateHelper.isTimeIntervalFromCurrentMonth(timeInterval: endTimeInterval)){
//                        let model: DateModel = DateModel()
                        
                        //verificar interesecoes
                        arrDateModel.dates.append(contentsOf: calculateIntersections(initialTime: startTimeInterval, finalTime: endTimeInterval, isMaintenance: false))
                            
                        arrTimeIntervals.append(startTimeInterval)
                        
                        
//                        model.timeInterval = startTimeInterval
//                        model.isMaintenance = false
//
//                        arrDateModel.dates.append(model)
                    }
                    
                }else{
                    
                    let initialTime = attr["INICIO_PREVISTO"] as! TimeInterval
                    let finalTime = attr["TERMINO_PREVISTO"] as! TimeInterval
                    
                    if(DateHelper.isTimeIntervalFromCurrentMonth(timeInterval: initialTime)){
                        let initialHour = DateHelper.getTimeFrom(timeInterval: initialTime)
                        let finalHour = DateHelper.getTimeFrom(timeInterval: finalTime)
                        
                        if((initialHour >= finalHour) && (arrTimeIntervals.contains(initialTime) == false) ){
                            arrDateModel.dates.append(contentsOf: calculateIntersections(initialTime: initialTime, finalTime: finalTime))
                            arrTimeIntervals.append(initialTime)
                        }else{
                            if(arrTimeIntervals.contains(initialTime) == false){
                                let model = DateModel()
                                model.initialTime = DateHelper.getTimeFrom(timeInterval: initialTime).asFormattedHour()
                                model.finalTime = DateHelper.getTimeFrom(timeInterval: finalTime).asFormattedHour()
                                model.isMaintenance = true
                                model.timeInterval = initialTime
                                
                                arrDateModel.dates.append(model)
                                arrTimeIntervals.append(initialTime)
                            }
                        }
                    }
                }
                
            }else{
                // rise error - no attributes key
            }
            
            
        }
        
        return arrDateModel
    }
    
    static func modelType() -> String {
        return MT_date
    }
    
    
}
