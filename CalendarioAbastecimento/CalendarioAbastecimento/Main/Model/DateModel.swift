//
//  DateModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class DateModel {
    var timeInterval: TimeInterval = TimeInterval()
    var isMaintenance: Bool = true
    var initialTime: String = "00:00"
    var finalTime: String = "23:59"
    
    func addScheduleInfo(initialTime: String?, finalTime: String? ){
        
        if let initial = initialTime{
            self.initialTime = initial
        }
        
        if let final = finalTime{
            self.finalTime = final
        }
    }
    
}
