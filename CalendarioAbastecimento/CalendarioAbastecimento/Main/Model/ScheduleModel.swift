//
//  ScheduleModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class ScheduleModel : ServiceModelProtocol {
    
    var scheduleInfo: String = ""
    
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol {
        let model = ScheduleModel()
        
        let features: [Dictionary<String, Any>] = dict["features"] as! [Dictionary<String, Any>]
        
        if let firstFeateures = features.first{
            let attr = firstFeateures["attributes"] as! Dictionary<String, Any>
            model.scheduleInfo = attr["HORARIO"] as! String
        }
        
        return model
    }
    
    static func modelType() -> String {
        return MT_schedule
    }
    
    
}
