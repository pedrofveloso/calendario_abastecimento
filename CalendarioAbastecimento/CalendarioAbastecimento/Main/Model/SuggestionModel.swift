//
//  SuggestionModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/10/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class SuggestionModel: ServiceModelProtocol {
    
    var cep: String = ""
    var magicKey: String = ""
    
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol {
        let model : SuggestionModel = SuggestionModel()
        let suggestions = dict["suggestions"] as! [Dictionary<String, Any>]
        
        if let firstSuggestion = suggestions.first{
            model.cep = firstSuggestion["text"] as! String
            model.magicKey = firstSuggestion["magicKey"] as! String
        }
        
        return model
    }
    
    static func modelType() -> String {
        return MT_suggestion
    }
    
    
    
}
