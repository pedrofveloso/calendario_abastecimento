//
//  PerimeterModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class PerimeterModel : ServiceModelProtocol {
    
    var perimiterID: String = ""
    
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol {
        let model = PerimeterModel()
        
        let features: [Dictionary<String, Any>] = dict["features"] as! [Dictionary<String, Any>]
        
        if let firstFeateures = features.first{
            let attr = firstFeateures["attributes"] as! Dictionary<String, Any>
            model.perimiterID = attr["ID"] as! String
        }
        
        return model
    }
    
    static func modelType() -> String {
        return MT_perimeter
    }
}
