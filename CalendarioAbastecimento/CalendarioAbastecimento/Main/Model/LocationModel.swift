//
//  LocationModel.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/7/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class LocationModel: ServiceModelProtocol{
    
    var x: String = ""
    var y: String = ""
    
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol {
        let model = LocationModel()
        
        let candidates: [Dictionary<String, Any>] = dict["candidates"] as! [Dictionary<String, Any>]
        
        
        if let firstLocation = candidates.first{
            let location = firstLocation["location"] as! Dictionary<String, Any>
            model.x = String(format:"%f", location["x"] as! Double)
            model.y = String(format:"%f", location["y"] as! Double)
        }
        
        return model
    }
    
    static func modelType() -> String {
        return MT_location
    }
    
    
    
}
