//
//  Address.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/11/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class AddressModel : NSObject, ServiceModelProtocol {
    var street:String = ""
    var neighborhood: String = ""
    var city: String = ""
    var cep: String = "" {
        didSet {
            if(cep.isEmpty == false){
                cep = "\(cep.prefix(5))-\(cep.suffix(3))"
            }
        }
    }
    
    static func convertDicToModel(dict: Dictionary<String, Any>) -> ServiceModelProtocol {
        let model = AddressModel()
        if let street = dict["logradouro"]{
            //republicaVirtual
            if let type = dict["tipo_logradouro"]{
                model.street = "\(type as! String) \(street as! String)"
            }else{ //viaCep
                model.street = street as! String
            }
        }
        
        if let neighbor = dict["bairro"]{
            model.neighborhood = neighbor as! String
        }
        //viaCep
        if let city = dict["localidade"]{
            model.city = city as! String
        }
        
        //republicaVirtual
        if let city = dict["cidade"]{
            model.city = city as! String
        }
        
        if let cep = dict["cep"]{
            model.cep = cep as! String
        }
        
        return model
    }
    
    static func convertModelToDict(model: AddressModel) -> Dictionary<String,String> {
        var dict = Dictionary<String, String>()
        dict["cep"] = model.cep
        dict["localidade"] = model.city
        dict["bairro"] = model.neighborhood
        dict["logradouro"] = model.street
        
        return dict
    }
    
    static func modelType() -> String {
        return MT_address
    }
    
    func description() -> String{
        return "\(self.street), \(self.neighborhood), \(self.city)"
    }
}
