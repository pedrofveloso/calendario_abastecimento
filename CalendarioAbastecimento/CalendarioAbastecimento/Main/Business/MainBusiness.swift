//
//  MainBusiness.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/3/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class MainBusiness : NSObject {
    
    static let instance = MainBusiness()
    private override init(){}
    
    func isCepValid(cep:String) -> Bool{
        var ret:Bool = false
        
        let maskedCep = "\(cep.prefix(5))-\(cep.suffix(3))"
        
        if(maskedCep.isEmpty == false){
            let regex = "^[0-9]{5}-[0-9]{3}$"
            ret = maskedCep.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
        }
        
        return ret && cep.count == 8
    }
}
