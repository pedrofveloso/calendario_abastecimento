//
//  FavoritesTableViewCell.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/12/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit

protocol FavoritesDelegate {
    func removeAddress(index: Int)
}

class FavoritesTableViewCell: UITableViewCell {
    @IBOutlet weak var lbCep: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    var delegate: FavoritesDelegate!
    var index: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(address: AddressModel){
        DispatchQueue.main.async {
            self.lbCep.text = address.cep
            self.lbAddress.text = "\(address.street) - \(address.neighborhood)"
        }
    }
    
    @IBAction func onTapDelete() {
        self.delegate.removeAddress(index: self.index)
    }
    
}
