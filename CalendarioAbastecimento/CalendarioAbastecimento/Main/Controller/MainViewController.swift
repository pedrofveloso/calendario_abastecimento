//
//  MainViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/3/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit
import UserNotifications


class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIToolbarDelegate, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, FavoritesDelegate{
    
    //    MARK:- IBOutlets    
    @IBOutlet weak var txfCep: UITextField!
    @IBOutlet weak var tbvCep: UITableView!
    @IBOutlet weak var btNotification: UIButton!
    @IBOutlet weak var vwContainer: UIView!
    
    //    MARK:- Variables
    var arrFavorites:[AddressModel] = [] 
    var toolbar: UIToolbar!
    var loading: LoadingViewController!
    
    //    MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arrFavorites = DataManager.getFavorites()
        self.setupNotifications()
        self.setupLocalNotificationButton()
        self.setupNavigation()
        self.setupDependencies()
        self.setupKeyboardToolbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //FIXME: - find a better way to call getFavorites, because at first time this is being called twice (viewDidLoad and here)
        self.arrFavorites = DataManager.getFavorites()
        DispatchQueue.main.async {
            self.tbvCep.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //    MARK:- TableViewMethods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrFavorites.isEmpty ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Favoritos"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerview = view as! UITableViewHeaderFooterView
        headerview.backgroundView?.backgroundColor = UIColor.white
        headerview.textLabel?.font = UIFont(name: FONT_trebuchetBold, size: 14)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFavorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELLID_favoriteCep) as! FavoritesTableViewCell
        cell.index = indexPath.row
        cell.delegate = self
        cell.loadData(address: self.arrFavorites[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.callLoading(cep: self.arrFavorites[indexPath.row].cep)
    }
    
//    MARK:- TextField Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.isValidToLimit(limit: 8, string: string, range: range)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(self.txfCep.isFirstResponder){
            self.txfCep.resetStyle()
            self.txfCep.resignFirstResponder()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.txfCep.resetStyle()
    }
    
    //    MARK:- Keyboard Methods
    @objc private func keyboardWillShow(notification: Notification){
        var info = notification.userInfo!
        
        if let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let navBarHeight = (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height
            let finalPoint = CGPoint(x: self.txfCep.frame.origin.x, y: navBarHeight + self.txfCep.frame.origin.y + self.txfCep.frame.height)
            
            if(keyboardRect.contains(finalPoint)){
                let delta = finalPoint.y - (keyboardRect.origin.y)
                self.view.frame = CGRect(x: self.view.bounds.origin.x,
                                         y: self.view.frame.origin.y - delta,
                                         width: self.view.frame.width,
                                         height: self.view.frame.height)
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification){
        let navBarHeight = (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height
        
        if(self.view.frame.origin.y != navBarHeight){
            self.view.frame = CGRect(x: self.view.bounds.origin.x,
                                     y: navBarHeight,
                                     width: self.view.frame.width,
                                     height: self.view.frame.height)
        }
    }
    
    //    MARK:- Setup Methods
    private func setupDependencies(){
        self.tbvCep.delegate = self
        self.tbvCep.dataSource = self
        
        self.txfCep.delegate = self
        
        self.tbvCep.register(UINib(nibName: "FavoritesTableViewCell", bundle: nil), forCellReuseIdentifier: CELLID_favoriteCep)
        
        self.view.backgroundColor = UIColor.blue_schedule
    }
    
    private func setupNavigation(){
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont(name: FONT_trebuchetBold, size: 18)!,
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.blue_schedule
    }
    
    private func setupKeyboardToolbar(){
        self.toolbar = UIToolbar()
        self.toolbar.delegate = self
        self.toolbar.barStyle = UIBarStyle.default
        self.toolbar.barTintColor = UIColor.blue_schedule
        self.toolbar.tintColor = UIColor.white
        self.toolbar.sizeToFit()
        
        let okBtn = UIBarButtonItem.init(title: "Ok", style: .done, target: self, action: #selector(onSearchCEP))
        let flexibleSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        self.toolbar.setItems([flexibleSpace, okBtn], animated: true)
        self.toolbar.isUserInteractionEnabled = true
        self.txfCep.inputAccessoryView = self.toolbar
    }
    
    @objc private func setupLocalNotificationButton(){
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "isFirstLaunch")
        if launchedBefore  {
            //Local Notification
            let notificationType = UIApplication.shared.currentUserNotificationSettings!.types
            if notificationType == [] {
                self.btNotification.isHidden = false
            } else {
                self.btNotification.isHidden = true
            }
        } else {
            UserDefaults.standard.set(true, forKey: "isFirstLaunch")
        }
        
        
        
        
    }
    
//    MARK:- Notification Methods
    private func setupNotifications(){
        // local notificaitons
        NotificationCenter.default.addObserver(self, selector: #selector(onReceivedCalendarModel), name: NT_onReceivedCalendar, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissLoading), name: NT_onReceivedError, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setupLocalNotificationButton), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        // keyboard notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "calendarSegue"){
            DispatchQueue.main.async {
                self.toggleNavigation(to: false, animated: false)
                let calendarModel = sender as! CalendarModel
                let calendarVC = segue.destination as! CalendarViewController
                self.dismiss(animated: true, completion: nil)
                calendarVC.calendarModel = calendarModel
            }
        }
    }
    
    private func toggleNavigation(to isHidden: Bool, animated: Bool){
        DispatchQueue.main.async {
            self.navigationController?.setNavigationBarHidden(isHidden, animated: animated)
        }
    }

    
    //    MARK:- Methods
    @objc private func onSearchCEP(){
        if let cep = self.txfCep.text{
            if(MainBusiness.instance.isCepValid(cep: cep) && (cep.isEmpty == false)){
                self.txfCep.resetStyle()
                self.callLoading(cep: cep)

            }else{
                self.txfCep.addInvalidStyle()
                
                let text = cep.count < 8 ? TEXT_popover_invalidLengthCEP : TEXT_popover_invalidCharCEP

                _ = AlertController.instance.showPopover(parent: self, sender: self.txfCep, height: CGFloat(HEIGHT_txCep), text: text, image: #imageLiteral(resourceName: "maintenanceIcon"))
            }

            self.txfCep.resignFirstResponder()
        }
    }
    
    @objc private func onReceivedCalendarModel(notification: Notification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.performSegue(withIdentifier: "calendarSegue", sender: notification.object)
        }
    }
    
    func removeAddress(index: Int) {
        AlertController.instance.showAlertWithCustomActions(parent: self, title: ALERT_titleRemoveFavorites, message: ALERT_messageRemoveFavorites, button1title: "Cancelar", button2completion: {
            DataManager.removeFromFavorites(address: self.arrFavorites[index])
            self.arrFavorites = DataManager.getFavorites()
            self.tbvCep.reloadData()
        })
    }
    
    private func callLoading(cep: String){
        let storyboard = UIStoryboard(name: "Loading", bundle: nil)
        self.loading = storyboard.instantiateInitialViewController() as? LoadingViewController
        loading.cepToSearch = cep
        self.toggleNavigation(to: true, animated: false)
        
        self.present(self.loading, animated: true, completion: {
            self.txfCep.text = ""
        })
    }
    
    @objc private func dismissLoading(notification:Notification){
        DispatchQueue.main.async {
            let dict = notification.object as! Dictionary<String, String>
            let alertTitle = dict["title"]
            let alertMessage = dict["message"]
            if(alertTitle?.isEmpty == false || alertMessage?.isEmpty == false){
                AlertController.instance.showAlertWithCustomActions(parent: self.loading, title: alertTitle!, message: alertMessage!, button2completion: {
                    self.toggleNavigation(to: false, animated: true)
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
//    MARK:- IBActions
    @IBAction func onActiveNotification() {
        DispatchQueue.main.async {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            settingsUrl.open()
        }
    }
    
    
//    MARK:- UIPopoverPresentationControllerDelegate Methods
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
