//
//  LoadingViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/12/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation
import UIKit

class LoadingViewController: UIViewController {
    
//    MARK:- IBOutlets
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var icon1: UIImageView!
    @IBOutlet weak var icon2: UIImageView!
    @IBOutlet weak var icon3: UIImageView!
    @IBOutlet weak var icon4: UIImageView!
    @IBOutlet weak var icon5: UIImageView!
    @IBOutlet weak var icon6: UIImageView!
    
//    MARK:- Properties
    var cepToSearch: String!
    var arrAssetsToLoad:[UIImageView] = []
    
//    MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrAssetsToLoad = [icon1, icon2, icon3, icon4, icon5, icon6]
        self.addNotificationListeners()
        
        self.view.backgroundColor = UIColor.blue_schedule
    }
    
    override func viewDidAppear(_ animated: Bool) {
        LoadingBusiness.instance.callAddressService(cep: self.cepToSearch)
        DispatchQueue.main.async {
            self.lbMessage.text = TEXT_loading_searchingAddress
        }
    }
    
//    MARK:- Notification Methods
    private func addNotificationListeners(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabelMessage), name: NT_onReceivedSuccess, object: nil)
    }
    
    
    @objc private func updateLabelMessage(notification:Notification){
        let iconIndex = notification.object as! Int

        DispatchQueue.main.async {
            self.lbMessage.text = self.selectTextToDisplay(index: iconIndex)
            
            UIView.animate(withDuration: 1.5, animations: {
                self.arrAssetsToLoad[iconIndex].alpha = 1
            })
        }
    }
    
    private func selectTextToDisplay(index: Int) -> String{
        switch index {
        case 1:
            return TEXT_loading_searchingAddress
        case 2:
            return TEXT_loading_localizingReservatory
        case 3:
            return TEXT_loading_searchingSchedule
        case 4:
            return TEXT_loading_searchingDates
        case 5:
            return TEXT_loading_creatingCalendar
        default:
            return TEXT_loading_validatingCEP
        }
    }
}
