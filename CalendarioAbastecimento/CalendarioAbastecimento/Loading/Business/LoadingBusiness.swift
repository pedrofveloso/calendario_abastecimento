//
//  LoadingBusiness.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/14/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import Foundation

class LoadingBusiness:NSObject {
    
    static let instance = LoadingBusiness()
    public override init() {}
    
    func callAddressService(cep: String, isRetry:Bool = false){
        let cleanedCep = cep.replacingOccurrences(of: "-", with: "")
        
        let serviceRequest = ServiceRequest.address(cep: cleanedCep, isRetry: isRetry)
        
        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 0)
        
        _ = MainService<AddressModel>().callService(serviceRequest: serviceRequest) { (error, model) in
            if(error == nil){
                if let address = model {
                    address.cep = cep
                    self.callSuggestionsService(address:address)
                    
                }else{
                    if(isRetry == false){
                        self.callAddressService(cep: cep, isRetry: true)
                    }else{
                       NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": ERR_noAddressErrorTitle, "message" : ERR_noAddressErrorMessage])
                    }
                }
                
            }else{
                if(isRetry == false){
                    self.callAddressService(cep: cep, isRetry: true)
                }else{
                    let err = error! as NSError
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
                }
            }
        }
    }
    
    private func callSuggestionsService(address:AddressModel){
        let addressText = address.description()
        let serviceRequest = ServiceRequest.suggestion(addressText: addressText)
        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 1)
        
        _ = MainService<SuggestionModel>().callService(serviceRequest: serviceRequest){ (error, model) in
            if(error == nil){
                if let suggestion = model {
                    self.callLocationService(address: address, with: suggestion.magicKey)
                }else{
                    //raise error no suggestions
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": ERR_noAddressErrorTitle, "message" : ERR_noAddressErrorMessage])
                }
            }else{
                let err = error! as NSError
                NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
            }
        }
    }
    
    
    private func callLocationService(address:AddressModel, with magicKey:String){
        
        let serviceRequest = ServiceRequest.location(addressText:address.description(), with:magicKey)
        
        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 2)
        
        _ = MainService<LocationModel>().callService(serviceRequest: serviceRequest) { (error, model) in
            if(error == nil){
                if let location = model{
                    self.callPerimeterService(location: location, passing:address)
                    
                }else{
                    //raise error location not found
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": ERR_noAddressErrorTitle, "message" : ERR_noAddressErrorMessage])
                }
                
            }else{
                let err = error! as NSError
                NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
            }
        }
    }
    
    
    private func callPerimeterService(location: LocationModel, passing address:AddressModel){
        
        let serviceRequest = ServiceRequest.perimeter(x: location.x, y: location.y)
        
        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 3)
        
        _ = MainService<PerimeterModel>().callService(serviceRequest: serviceRequest) { (error, model) in
            if(error == nil){
                if let perimeter = model{
                    
//                    self.callDatesService(perimeterID: perimeter.perimiterID, scheduleModel: <#T##ScheduleModel#>, address: address)
                    
                    self.callSchedulesService(perimeterID: perimeter.perimiterID, passing:address)
                    
                }else{
                    //raise error - perimeter not founded
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : ERR_serviceErrorMessage])
                }
                
            }else{
                let err = error! as NSError
                NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
            }
        }
    }
    
    private func callSchedulesService(perimeterID: String, passing address:AddressModel){
        let serviceRequest = ServiceRequest.schedules(perimeterID: perimeterID)

//        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 3)

        _ = MainService<ScheduleModel>().callService(serviceRequest: serviceRequest) { (error, model) in
            if(error == nil){
                if let schedule = model{
                    self.callDatesService(perimeterID: perimeterID, scheduleModel: schedule, address: address)

                }else{
                    //raise error - schedule not found
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : ERR_serviceErrorMessage])
                }

            }else{
                let err = error! as NSError
                NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
            }
        }
    }
    
    private func callDatesService(perimeterID: String, scheduleModel: ScheduleModel, address:AddressModel){
        
        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 4)
        
        let serviceRequest = ServiceRequest.validDates(perimeterID: perimeterID, month:DateHelper.getCurrentDateWith(format: "MM"), year: DateHelper.getCurrentDateWith(format: "yyyy"))
        
        _ = MainService<ArrayDateModel>().callService(serviceRequest: serviceRequest) { (error, model) in
            if(error == nil){
                if let arr = model{
                    
                    if(scheduleModel.scheduleInfo.isEmpty == false){
                        arr.addSchedulesToDates(scheduleModel: scheduleModel)
                    }
                    
                    let calendarModel: CalendarModel = CalendarModel()
                    calendarModel.mapDates(arrayDataModel:arr)
                    calendarModel.address = address
                    self.callMaintenanceService(perimeterID: perimeterID, calendarModel: calendarModel)
                    
                    
                }else{
                    //raise error - schedule not found
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : ERR_serviceErrorMessage])
                    
                }
                
            }else{
                let err = error! as NSError
                NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
            }
        }
    }
    
    private func callMaintenanceService(perimeterID: String, calendarModel: CalendarModel){
        let serviceRequest = ServiceRequest.maintenanceDates(perimeterID: perimeterID)
        
        NotificationCenter.default.post(name: NT_onReceivedSuccess, object: 5)
        
        _ = MainService<ArrayDateModel>().callService(serviceRequest: serviceRequest) { (error, model) in
            if(error == nil){
                if let arr = model {
                    
                    calendarModel.addMaintenanceDates(maintenanceDates: arr)
                    
                    if(calendarModel.map.keys.isEmpty){
                        NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": ALERT_titleNoDates, "message" : ALERT_messageNoDates])
                        
                    }else{
                        NotificationCenter.default.post(name: NT_onReceivedCalendar, object: calendarModel)
                    }
                    
                }else{
                    NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : ERR_serviceErrorMessage])
                }
            }else{
                let err = error! as NSError
                NotificationCenter.default.post(name: NT_onReceivedError, object: ["title": "Ops!", "message" : err.domain])
            }
        }
    }
}
