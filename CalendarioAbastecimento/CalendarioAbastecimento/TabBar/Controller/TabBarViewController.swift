//
//  TabBarViewController.swift
//  CalendarioAbastecimento
//
//  Created by Veloso, Pedro Danilo on 3/16/18.
//  Copyright © 2018 LifeMiles. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController, UITabBarDelegate {
    
    @IBOutlet weak var tabBar: UITabBar!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.delegate = self
        
        self.tabBar.barTintColor = UIColor.blue_schedule
        self.tabBar.tintColor = UIColor.white
        self.tabBar.unselectedItemTintColor = UIColor.white

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TabBar
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0){
            openAbout()
        } else{
            openReportBug()
        }
    }
    
    private func openAbout(){
        let storyboard = UIStoryboard(name: "About", bundle: nil)
        let aboutVC = storyboard.instantiateInitialViewController() as! AboutViewController
        self.parent?.present(aboutVC, animated: true, completion: nil)
    }
    
    private func openReportBug(){
        let storyboard = UIStoryboard(name: "ReportError", bundle: nil)
        let aboutVC = storyboard.instantiateInitialViewController() as! ReportErrorViewController
        self.parent?.present(aboutVC, animated: true, completion: nil)
    }

}
